/*
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * (c) 2021 Juergen Borleis <projects@caps-printing.org>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 */
#include <assert.h>
#include <math.h>
#include <errno.h>
#include <stdint.h>

#include "qldriver.h"

/**
 * @file qlcolour.c
 * @brief Creating the printer's wire data for bi-coloured print
 * @copyright GNU General Public License 2.0 or later
 * @author Jürgen Borleis
 *
 * The QL8xx familiy of printers is capable of printing red and black dots on a special medium
 * (e.g. DK2251).
 *
 * "Colour" here means: black and red on white labels.
 *
 * In this mode every line must be sent twice. First for low energie to print red dots and second
 * for high energie to print black dots.
 *
 * The result isn't perfect, since low energy creates red dots, the high energy black dots all
 * have red shadows around them (because the high energy cools down beside the main dot area, the
 * medium tends to red colour). Refer @ref bi_colour_limitation for details.
 */

extern int terminate;

/**
 * Value to force a black dot. Used in the printer's command to print the black dots, refer #colour_line_start for details
 */
#define HIGH_ENERGY 0x01
/**
 * Value to force a red dot. Used in the printer's command to print the red dots, refer #colour_line_start for details
 */
#define LOW_ENERGY 0x02

/**
 * Printer command (one part of it) to print bi-coloured
 */
struct colour_line_start {
	uint8_t command;	/**< 'w' */
	uint8_t mode; /**< #HIGH_ENERGY for high energy , #LOW_ENERGY for low energy */
	uint8_t cnt; /**< Count of bytes following */
	uint8_t data[0]; /**< Line data as payload */
} __packed;

/**
 * Remove dots in the @b black block, if they are already set in the @b red block
 * @param[in] cnt Element count in @b black_block and @b red_block
 * @param[in,out] black_block Monochrome data for black dots
 * @param[in] red_block Monochrome data for red dots
 *
 * We should not send the command for low energy and high energy at the same
 * dot position. So, remove the black dots (high energy) in favour of the red
 * dots (low energy) if they overlap.
 *
 * @pre Both blocks must have the same data size
 */
static void remove_overlapping_dots(size_t cnt, unsigned char black_block[__restrict cnt], const unsigned char red_block[__restrict cnt])
{
	size_t u;

	for (u = 0; u < cnt; u++)
		black_block[u] &= (unsigned char)(~red_block[u]);
}

/**
 * Create the command to print one dual colour line
 * @param[out] cmd Command data structure where to store the line data
 * @param[in] input_cnt Count of signed shorts in @b inputb and @b inputr (max. 2040 elements, usual 720 elements)
 * @param[in] inputb Black data to convert
 * @param[in] inputr Red data to convert
 *
 * The printer wire colour data format is:
 *
@verbatim
  | command | <bytes_per_line> | command | <bytes_per_line> |
@endverbatim
 *
 * e.g. according to the QL800 datasheet 186 bytes over all (90 bytes per line each).
 */
static __nonnull((1,3,4)) void colour_line_convert(struct line_command *cmd, size_t input_cnt, const signed short inputb[__restrict input_cnt], const signed short inputr[__restrict input_cnt])
{
	struct colour_line_start *red_block, *black_block = cmd->command.cl;

	assert(input_cnt < 2041);
	assert((input_cnt % 8) == 0);

	/* do some math to get the pointer to the red buffer */
	red_block = (struct colour_line_start *)((uintptr_t)black_block + sizeof(struct colour_line_start) + cmd->bytes_per_line);

	transform_to_monochrome(input_cnt, inputb, black_block->data);
	black_block->command = 'w';
	black_block->mode = HIGH_ENERGY; /* High energy for black dots */
	black_block->cnt = (uint8_t)(input_cnt / 8);

	transform_to_monochrome(input_cnt, inputr, red_block->data);
	red_block->command = 'w';
	red_block->mode = LOW_ENERGY; /* Low energy for red dots */
	red_block->cnt = (uint8_t)(input_cnt / 8);

	/* Avoid black and red dots at the same location */
	remove_overlapping_dots(input_cnt / 8, black_block->data, red_block->data);
}

/**
 * Allocate the buffer for the colour print command
 * @param[in] bytes_per_line Expected bytes per line to be sent to the printer
 * @return #line_command structure
 *
 * @todo shared component, because the monochrome usecase is only a special usecase of the colour usecase
 */
static struct line_command *colour_command_get(size_t bytes_per_line)
{
	size_t buffer_cnt = sizeof(struct line_command) + 2 * (sizeof(struct colour_line_start) + bytes_per_line);
	struct line_command *cmd;

	assert(bytes_per_line < 256);

	cmd = malloc(buffer_cnt);
	if (cmd == NULL)
		exit(1); /* no hope */

	cmd->bytes_per_line = bytes_per_line;
	cmd->bytes_per_command = 2 * (sizeof(struct colour_line_start) + bytes_per_line); /* would be 1 * … for the monochrome usecase */
	cmd->command.cl = (struct colour_line_start *)&cmd[1];

	return cmd;
}

/**
 * Free the buffer for the monochrome print command
 * @param[in] cmd The command buffer to be freed
 */
static void colour_command_put(void *cmd)
{
	free(cmd);
}

/**
 * Process one line and send it to the printer
 * @param[in] ql Full job description
 * @param[in] cnv_black Black data half-tone converter
 * @param[in] cnv_red Red data half-tone converter
 * @retval 0 On success
 *
 * @pre The half tone converters must already be filled with two lines
 * @pre One of the three available half-tone algorithms must already be selected
 */
static __nonnull((1,2,3)) int cups_ql_colour_line_process(struct qldriver *ql, struct halftone_converter * __restrict cnv_black, struct halftone_converter * __restrict cnv_red)
{
	struct line_command *cmd;
	int rc;

	switch (ql->half_tone_type) {
	case HT_ERROR_DIFFUSION:
		halftone_line_with_error_diffusion(cnv_black);
		halftone_line_with_error_diffusion(cnv_red);
		break;
	case HT_ORDERED:
		halftone_line_ordered(cnv_black);
		halftone_line_ordered(cnv_red);
		break;
	case HT_NONE:
		halftone_line_no_dither(cnv_black);
		halftone_line_no_dither(cnv_red);
		break;
	default:
		caps_print_error(_("Unkown half tone type: %u. Cannot continue.\n"), ql->half_tone_type); /* doc */
		exit(1);
	}

	cmd = colour_command_get(ql->bytes_per_line);

	colour_line_convert(cmd, cnv_black->pixel_count, cnv_black->sliding_lines[0], cnv_red->sliding_lines[0]);
	rc = ql_printer_data_send(ql, cmd->command.bw, cmd->bytes_per_command, 0);

	colour_command_put(cmd);
	return rc;
}

/**
 * Return the fractional part of a floating point
 * @param[in] no The full number
 * @return the fractional part of @b no
 *
 * @todo can lrint() be used here instead?
 */
static double fraction(double no)
{
	return no - (double)((long)no);
}

/**
 * Convert one pixel from RGB colour space to HSV colour space
 * @param[out] hsv The target HSV colour space
 * @param[in] rgb_pixel The RGB pixel to convert
 *
 * - all three components equal to '0' will print a black dot.
 * - all three components equal to '0xff' will print no dot.
 *
 * @note If the colour is on the vertical axis (e.g. a shade of grey), the #qlhsv::hue component is
 *       returned as a negative number to mark it as invalid/undefined/do-not-use.
 *       But the #qlhsv::value component is still valid in this case.
 *
 * https://github.com/iamh2o/rgbw_colorspace_converter/ @n
 * https://www.neltnerlabs.com/saikoled/how-to-convert-from-hsi-to-rgb-white
 */
static __nonnull((1,2)) void rgb2hsv(struct qlhsv *hsv, const struct qlrgb *rgb_pixel)
{
	static const double onesixth = 1.0 / 6.0;
	static const double onethird = 1.0 / 3.0;
	static const double twothird = 2.0 / 3.0;
	double dred, dgreen, dblue;
	double min, max, diff;

	/* Catch the vertical axis, e.g. black to white grey scale to avoid all the calculations */
	if (rgb_pixel->red == rgb_pixel->green && rgb_pixel->green == rgb_pixel->blue) {
		hsv->hue = -1.0; // mark as undefined
		hsv->saturation = 0.0;
		hsv->value = (double)rgb_pixel->red;
		return;
	}

	/* from here on, at least two of the three colour components differ */

	dred = (double)rgb_pixel->red;
	dgreen = (double)rgb_pixel->green;
	dblue = (double)rgb_pixel->blue;

	dred /= 255.0;
	dgreen /= 255.0;
	dblue /= 255.0;

	min = dred;
	if (isless(dgreen, min))
		min = dgreen;
	if (isless(dblue, min))
		min = dblue;
	max = dred;
	if (isgreater(dgreen, max))
		max = dgreen;
	if (isgreater(dblue, max))
		max = dblue;

	if (islessgreater(min, max)) {
		diff = max - min;

		if (iszero(diff)) { // on the vertical axis
			hsv->hue = -1.0; // undefined
			assert(0); // should not happen
		} else if (!islessgreater(max, dred)) {
			hsv->hue = onesixth * ((dgreen - dblue) / diff) + 1.0;
			hsv->hue = fraction(hsv->hue);
		} else if (!islessgreater(max, dgreen))
			hsv->hue = onesixth * ((dblue - dred) / diff) + onethird;
		else if (!islessgreater(max, dblue))
			hsv->hue = onesixth * ((dred - dgreen) / diff) + twothird;
		else {
			hsv->hue = -1.0; // undefined
			assert(0);
		}

		if (iszero(max))
			hsv->saturation = 0.0;
		else
			hsv->saturation = 1.0 - (min / max);
	} else {
		/* min and max are equal -> grey scale, should not happen */
		hsv->hue = -1.0;
		hsv->saturation = 0.0;
		hsv->value = max;
		assert(0); // should not happen
	}

	hsv->hue *= 360.0; // degree
	hsv->saturation *= 255.0;
	hsv->value = max * 255.0;
}

/**
 * Mark a red dot value into both line bufffers
 * @param[in] val The brightness of the dot (-255 = full red brightness … 0 = no colour) (@b negative value!)
 * @param[out] black_pixel Where to store the corresponding black value for no black dot at this position
 * @param[out] red_pixel Where to store the corresponding red value
 *
 * The input values for the red dots get inverted. A 0x00 input means "no red", so a 255 is stored to
 * keep the paper colour. And a -255 input means a "full red", so a 0 is stored to print a red dot.
 * At the same position, the black pixel is setup to keep the paper colour and to not disturb the
 * red dot printing.
 */
static __nonnull((2,3)) void red_dot_mark(signed int val, signed short * __restrict black_pixel, signed short * __restrict red_pixel)
{
	assert(val <= 0 && val > -256);

	*black_pixel = COLOUR_VAL_BRIGHT; // Value for "do not print a dot, keep the paper colour"
	*red_pixel = (signed short)(COLOUR_VAL_BRIGHT + val); // Value for "print a red dot"
}

/**
 * Mark a black dot value into both line bufffers
 * @param[in] val The brightness of the dot (-255 = full brightness … 0 = black) (@b negative value!)
 * @param[out] black_pixel Where to store the corresponding black value for no black dot at this position
 * @param[out] red_pixel Where to store the corresponding red value
 *
 * The black dots scale from 0x00 (black dot!) to 0xff (paper colour!)
 * Red settings depends on #red_dot_mark() and its inversion, e.g. both colours scale from 0x00 (dot!) to 0xff (no dot!)
 */
static __nonnull((2,3)) void black_dot_mark(signed int val, signed short * __restrict black_pixel, signed short * __restrict red_pixel)
{
	assert(val >= 0 && val < 256);

	*black_pixel = (signed short)val; // Value for "print a black dot"
	*red_pixel = COLOUR_VAL_BRIGHT; // Value for "do not print a dot, keep the paper colour"
}

/**
 * Convert an RGB pixel to grey scale
 * @param[in] pixel The pixel in RGB to convert (scale is 0…255 in each channel)
 * @return Grey value of the coloured RGB pixel (0…255)
 *
 * @pre All colour component values are scaled from 0x00 … 0xff.
 *      Still expected scale here is: '0' no-colour, 0xff full colour
 *
 * https://duckduckgo.com/?q=color%20picker%20%23808080
 */
static __nonnull((1)) signed int to_grey(const struct qlrgb *pixel)
{
	double red, green, blue;
	red = (double)pixel->red;
	green = (double)pixel->green;
	blue = (double)pixel->blue;
	return (signed int)(red * 0.2125 + green * 0.7154 + blue * 0.0721);
}

/**
 * Check, if the given pixel in RGB colour space defines a red pixel
 * @param[in] ql Full job description
 * @param[in] pixel The RGB pixel to check
 * @retval Positive Black pixel brightness (grey scale value)
 * @retval Negative Red pixel brightness (hsv's value)
 *
 * This is a simple and ugly red dot detection. Since we have an RGB colour space, a red colour is
 * hard to detect. Converting it into an HSV colour space makes it simpler to detect red.@n If the
 * @b hue is below 30° or above 350° it is more or less a red. It then depends on its @b saturation and
 * @b value if it is a visible red - or still a black.@n
 * All checked values are user defined and part of the #qldriver structure.
 */
static __nonnull((1,2)) signed int is_red(struct qldriver *ql, const struct qlrgb *pixel)
{
	struct qlhsv hsv;
	signed int grey;

	grey = to_grey(pixel);
	rgb2hsv(&hsv, pixel);

	assert(isless(hsv.value, 256.0));
	assert(isless(hsv.saturation, 256.0));

	if (signbit(hsv.hue))
		return grey; /* Special case: some shade of grey */

	// If hue is 0°…30° or 350°…360° we can talk about some kind of *red*
	if (isgreater(hsv.hue, ql->lower_red_angle) && isless(hsv.hue, ql->upper_red_angle))
		return grey; // no, still grey

	/* detecting a valuable red dot via saturation and value is a non linear function, try with a rectangle instead */
	if (isless(hsv.saturation, ql->lower_red_sat) || isless(hsv.value, ql->lower_red_val))
		return grey; /* a very dark red, keep it a black dot */

	return (signed int)(-hsv.value); /* it is a red dot */
}

/**
 * Read in the next line from a CUPS RGB raster right border aligned and separate black (e.g. shades of grey) and red (shades of)
 * @param[in] ql Full job description
 * @param[in,out] cnv_black The black part of the line
 * @param[in,out] cnv_red The red part of the line
 *
 * @pre Input Raster is RGB 'chunked', 8 bit per channel (e.g. RGB RGB RGB …)
 *
 * @todo The input raster can be smaller than the required line width. In this
 *       case the labels are smaller (for example) and the raster must be right
 *       aligned.
 *
 * @note Both colours gets scaled to 0x00 (dot) to 0xff (no dot) in both halftone converters
 */
static __nonnull((1,2,3)) int cups_ql_next_colour_line_read(struct qldriver *ql, struct halftone_converter * __restrict cnv_black, struct halftone_converter * __restrict cnv_red)
{
	unsigned char b[ql->trim.raster_width * sizeof(struct qlrgb)] __nonstring;
	struct qlrgb *pixel = (struct qlrgb *)b;
	signed short *black_line, *red_line;
	signed int ret;
	size_t u, s;

	/* add data from the CUPS raster data */
	ret = cups_ql_next_line_read(ql, ql->trim.raster_width * 3, b);
	if (ret < 0)
		return ret;

	/* 'Throw away' the current top line by swapping both buffers */
	black_line = cnv_black->sliding_lines[0];
	cnv_black->sliding_lines[0] = cnv_black->sliding_lines[1];
	cnv_black->sliding_lines[1] = black_line;

	red_line = cnv_red->sliding_lines[0];
	cnv_red->sliding_lines[0] = cnv_red->sliding_lines[1];
	cnv_red->sliding_lines[1] = red_line;

	assert(ql->trim.left_offset + ql->trim.left_keep <= cnv_black->pixel_count);
	assert(ql->trim.left_skip + ql->trim.left_keep <= ql->trim.raster_width);

	/* Clear the whole line, e.g. nothing to print for now */
	for (u = 0; u < cnv_black->pixel_count; u++)
		black_line[u] = red_line[u] = COLOUR_VAL_BRIGHT;
	/*
	 * Start at this offset in the line to add pixels from the raster line.
	 * The remaining pixel are then right aligned.
	 */
	s = cnv_black->pixel_count - ql->trim.left_keep;

	/* Enter the real content correcly right aligned into the line */
	for (u = 0; u < ql->trim.left_keep; u++) {
		ret = is_red(ql, &pixel[u + ql->trim.left_skip]);
		if (ret < 0)
			red_dot_mark(ret, &black_line[u + s], &red_line[u + s]);
		else
			black_dot_mark(ret, &black_line[u + s], &red_line[u + s]);
	}

	return 0;
}

/**
 * Convert the current page into the printer's wire data format
 * @param[in] ql Full job description
 * @param[in] first_time '1' if called the first time of this job, '0' else
 * @retval 0 On success
 * @retval -EINVAL Unsupported raster input format
 * @retval -ECANCELED Termination request from outerspace
 *
 * The routine loops through all or the remaining lines of the current page and
 * converts them into the printer's colour wire format
 *
 * @pre The format was already checked, e.g. it fits into the printer's media.
 */
int cups_ql_colour_page_print(struct qldriver *ql, int first_time)
{
	struct halftone_converter cnv_black, cnv_red;
	int rc = -EINVAL;
	unsigned line;

	caps_print_loud(_("Entering bi-colour page print\n"));

	/* Init the half tone rasterizers */
	cnv_black.pixel_count = cnv_red.pixel_count = ql->bytes_per_line * 8; /* print head's dots */
	cnv_black.dotval[0] = cnv_red.dotval[0] = 0x00;
	cnv_black.dotval[1] = cnv_red.dotval[1] = 0xff;
	cnv_black.threshold = cnv_red.threshold = 0x80;
	sliding_halftone_get(&cnv_black);
	sliding_halftone_get(&cnv_red);

	assert(ql->trim.top_keep >= 2);

	/* Send the required job header when called the first time in this job */
	if (first_time) {
		rc = ql_job_header_generate(ql);
		if (rc < 0) {
			caps_print_error(_("Monochrome page print failed in job header\n"));
			return rc;
		}
	}

	/* Send the required page header on each call */
	rc = ql_page_header_generate(ql, first_time);
	if (rc < 0) {
		caps_print_error(_("Monochrome page print failed in page header\n"));
		return rc;
	}

	/* Read in the first line in a separate manner, because we need two lines to start the half tone algorithm */
	rc = cups_ql_next_colour_line_read(ql, &cnv_black, &cnv_red);
	if (rc == 0) {
		/* One loop per page */
		for (line = 1; line < ql->trim.top_keep; line++) {
			rc = cups_ql_next_colour_line_read(ql, &cnv_black, &cnv_red);
			if (rc < 0)
				break;
			rc = cups_ql_colour_line_process(ql, &cnv_black, &cnv_red);
			if (rc < 0)
				break;

			if (terminate) {
				rc = -ECANCELED;
				break;
			}
		}
	}

	if (rc == 0) {
		/* The last line of the raster is still in the converter as its bottom line. Process it as well */
		move_in_empty_line(&cnv_black, COLOUR_VAL_BRIGHT);
		move_in_empty_line(&cnv_red, COLOUR_VAL_BRIGHT);
		rc = cups_ql_colour_line_process(ql, &cnv_black, &cnv_red);
	}

	if (rc < 0) {
		/*
		 * Deal with some kind of processing failure and cancel the job if possible:
		 * - on a printer data error, we are out of chance here to send any further data.
		 *   - we need to give up here
		 * - on an input data error, the printer is in a state, where it awaits the next command.
		 *   - here we can send the reset command to cancel the job at the printer's side
		 *
		 * TODO distinguish the failure.
		 */
		rc = ql_reset(ql);
	}

	sliding_halftone_put(&cnv_black);
	sliding_halftone_put(&cnv_red);

	caps_print_loud(_("Bi-colour page print done\n"));
	return rc;
}

#ifdef TEST
/* Make internal functions available for testing */

int test_cups_ql_next_colour_line_read(struct qldriver *ql, struct halftone_converter *cnv_black, struct halftone_converter *cnv_red)
{
	return cups_ql_next_colour_line_read(ql, cnv_black, cnv_red);
}

signed int test_is_red(struct qldriver *ql, const struct qlrgb *pixel)
{
	return is_red(ql, pixel);
}

void test_rgb2hsv(struct qlhsv *hsv, const struct qlrgb *rgb_pixel)
{
	return rgb2hsv(hsv, rgb_pixel);
}

void test_cups_ql_colour_line_process(struct qldriver *ql, struct halftone_converter *cnv_black, struct halftone_converter *cnv_red)
{
	cups_ql_colour_line_process(ql, cnv_black, cnv_red);
}

void test_red_dot_mark(signed int val, signed short *black_pixel, signed short *red_pixel)
{
	red_dot_mark(val, black_pixel, red_pixel);
}

void test_black_dot_mark(signed int val, signed short *black_pixel, signed short *red_pixel)
{
	black_dot_mark(val, black_pixel, red_pixel);
}

void test_transform_to_monochrome(size_t input_cnt, const signed short input[input_cnt], unsigned char *data)
{
	transform_to_monochrome(input_cnt, input, data);
}

#endif
