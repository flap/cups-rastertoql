#
# Create a PDF for the DK1208 label
# - pre-cut
# - 38 mm x 90 mm
# - portrait orientation on the roll
#
from fpdf import *

# system dependent
fpdf.set_global('SYSTEM_TTFONTS', '/usr/share/fonts/truetype/')

WIDH = 38
HEIGHT = 90
LEFTM = 1.0
RIGHTM = 1.0
TOPM = 3.0
BOTM = 3.0

# ---------------------------------------------------------------
# Printing is "short edge first" and thus, 3 mm margin at top
# and bottom.

pdf = FPDF(orientation = 'P', unit = 'mm', format = (WIDH, HEIGHT))
pdf.set_margins(left = LEFTM, top = TOPM, right = RIGHTM)
pdf.set_auto_page_break(auto = True, margin = BOTM)

pdf.add_font('generic', '', 'lato/Lato-Heavy.ttf', uni = True)
pdf.set_draw_color(r = 0, g = 0, b = 0)
pdf.set_fill_color(r = 240, g = 240, b = 240)
pdf.set_font('generic', size = 16)

pdf.add_page()
pdf.rect(x = LEFTM, y = TOPM, w = WIDH - (LEFTM + RIGHTM), h = HEIGHT - (TOPM + BOTM), style = 'FD')
pdf.multi_cell(w = WIDH - 4, h = 12, txt = 'Hello World!\nDK1208', align = 'C')

# ---------------------------------------------------------------
# Printing is still "short edge first" and thus, 3 mm margin at top
# and bottom, but due to the rotation "Minus90", the left and right
# edges require the 3 mm margin.

pdf.add_page(orientation = 'L', format = (WIDH, HEIGHT))
pdf.set_margins(left = TOPM, top = RIGHTM, right = BOTM)
pdf.set_auto_page_break(auto = True, margin = LEFTM)

pdf.rect(x = TOPM, y = RIGHTM, w = HEIGHT - (TOPM + BOTM), h = WIDH - (LEFTM + RIGHTM), style = 'FD')
pdf.multi_cell(w = HEIGHT - 4, h = 12, txt = 'Hello World!\nDK1208', align = 'C')

pdf.output('dk1208.pdf', 'F')
